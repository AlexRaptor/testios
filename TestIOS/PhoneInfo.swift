//
//  PhoneInfo.swift
//  TestIOS
//
//  Created by Александр Селиванов on 16/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

struct PhoneInfo: Decodable {
    
    let region: PhoneRegion
    let oper: PhoneOperatorInfo
}

struct PhoneRegion: Decodable {
    
    let name: String
    let okrug: String
}

struct PhoneOperatorInfo: Decodable {
    
    let name: String
    let brand: String
}
