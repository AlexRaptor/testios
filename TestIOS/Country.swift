//
//  CountryCode.swift
//  TestIOS
//
//  Created by Александр Селиванов on 15/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

struct Country: Decodable {
    
    let name: String
    let dialCode: String
    let code: String
}
