//
//  MainViewController.swift
//  TestIOS
//
//  Created by Александр Селиванов on 15/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

protocol SelectCountryDelegate {
    func setCurrentCountry(country: Country)
}

class MainViewController: UIViewController {
    
    @IBOutlet var countryCodeButton: UIButton!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var searchButton: UIButton!

    private var countries = [Country]()
    private var currentCountry = Country(name: "Russia", dialCode: "+7", code: "RU") // это можно хранить в UserDefaults
    private var phoneInfoArray: [String]?

    override func viewDidLoad() {        
        super.viewDidLoad()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGestureRecognizer)

        phoneNumberTextField.delegate = self
        
        customizeUI()
        
        updateUI()
    }

    private func customizeUI() {
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.largeTitleDisplayMode = .always
        }
        
        countryCodeButton.layer.borderWidth = 1
        countryCodeButton.layer.borderColor = countryCodeButton.tintColor.cgColor
        countryCodeButton.layer.cornerRadius = 4

        searchButton.isEnabled = false
    }

    private func updateUI() {
        countryCodeButton.setTitle(currentCountry.dialCode, for: .normal)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "SelectCountry":
            let destinationVC = segue.destination as! CountryViewController
            destinationVC.countries = countries
            destinationVC.currentCountryDialCode = currentCountry.dialCode
            destinationVC.selectCountryDelegate = self
        case "ShowPhoneInfo":
            let destinationVC = segue.destination as! PhoneInfoViewController
            destinationVC.phoneNumber = "\(currentCountry.dialCode)\(phoneNumberTextField.text ?? "")"
            destinationVC.country = currentCountry.name
            destinationVC.phoneInfo = phoneInfoArray
        default:
            break
        }
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }

    private func showErrorAlert(message: String) {
        
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alert.addAction(action)

        present(alert, animated: true)
    }

    // MARK: - Actions

    @IBAction func countryCodeButtonTapped(_ sender: Any) {
        
        activityIndicator.startAnimating()

        NetworkManager.sendRequest(url: "https://raw.githubusercontent.com/isaac-weisberg/idsi-ios-development-test-task/master/com.idsiapps.whodatboi.dataset.countries.json", completion: { countryCodes, error in

            DispatchQueue.main.async {
                
                self.activityIndicator.stopAnimating()

                guard error == nil else {
                    self.showErrorAlert(message: error!.localizedDescription)
                    return
                }

                guard let countryCodes = countryCodes else { return }
                self.countries = countryCodes

                self.performSegue(withIdentifier: "SelectCountry", sender: self)
            }
        } as (([Country]?, Error?) -> Void))
    }

    @IBAction func searchButtonTapped(_ sender: UIButton) {
        
        activityIndicator.startAnimating()

        let phoneInfoManager = PhoneInfoManager()

        phoneInfoManager.process(phoneNumber: "\(currentCountry.dialCode)\(phoneNumberTextField.text ?? "")") { infoStringArray, error in

            DispatchQueue.main.async {
                
                self.activityIndicator.stopAnimating()

                guard error == nil else {
                    self.showErrorAlert(message: error!.localizedDescription)
                    return
                }

                guard let infoStringArray = infoStringArray else { return }

                self.phoneInfoArray = infoStringArray
                self.performSegue(withIdentifier: "ShowPhoneInfo", sender: self)
            }
        }
    }
}

// MARK: - SelectCountryDelegate Methods

extension MainViewController: SelectCountryDelegate {
    
    func setCurrentCountry(country: Country) {
        currentCountry = country
        updateUI()
    }
}

// MARK: - UITextFieldDelegate Methods

extension MainViewController: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchButton.isEnabled = false
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField.text?.count ?? 0) > 0 {
            searchButton.isEnabled = true
        } else {
            searchButton.isEnabled = false
        }
    }
}
