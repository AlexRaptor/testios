//
//  PhoneInfoManager.swift
//  TestIOS
//
//  Created by Александр Селиванов on 16/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

class PhoneInfoManager: UnknownAPIProtocol {
    
    private let serviceUrl = "https://htmlweb.ru/json/mnp/phone/"
    
    func process(phoneNumber: String, callback: @escaping ([String]?, Error?) -> Void) {
        
        let url = "\(serviceUrl)\(phoneNumber)"
        
        NetworkManager.sendRequest(url: url, completion: { (phoneInfo, error) in
            
            if error == nil {
                
                if let phoneInfo = phoneInfo {
                    
                    var phoneInfoArray = [String]()
                    phoneInfoArray.append("Оператор: \(phoneInfo.oper.brand)")
                    phoneInfoArray.append("Юр. лицо: \(phoneInfo.oper.name)")
                    phoneInfoArray.append("Регион: \(phoneInfo.region.name)")
                    phoneInfoArray.append("Округ: \(phoneInfo.region.okrug)")
                    
                    callback(phoneInfoArray, nil)
                } else {
                    callback(nil, nil)
                }
            } else {
                callback(nil, error)
            }
        } as (PhoneInfo?, Error?) -> Void)
    }
}
