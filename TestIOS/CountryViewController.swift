//
//  CountryViewController.swift
//  TestIOS
//
//  Created by Александр Селиванов on 15/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

class CountryViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var countries = [Country]()
    private var filteredCountries = [Country]()

    var currentCountryDialCode: String!
    var selectCountryDelegate: SelectCountryDelegate?
    
    private var searchText = "" {
        didSet {
            filterCountries()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
        
        filterCountries()
        
        customizeUI()
    }
    
    private func customizeUI() {
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .automatic
        }
    }
    
    private func filterCountries() {
        
        filteredCountries = searchText.isEmpty
            ? countries
            : countries.filter { $0.name.lowercased().contains(searchBar.text!.lowercased()) }
    }

}

// MARK: - UITableViewDataSource Methods

extension CountryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryTableViewCell

        configureTableCell(cell: cell, country: filteredCountries[indexPath.row])
        
        return cell
    }
    
    private func configureTableCell(cell: CountryTableViewCell, country: Country) {
        
        cell.countryNameLabel.text = country.name
        cell.countryDialCodeLabel.text = country.dialCode
        
        if country.dialCode == currentCountryDialCode {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let countryCode = filteredCountries[indexPath.row]
        
        currentCountryDialCode = countryCode.dialCode
        selectCountryDelegate?.setCurrentCountry(country: countryCode)
        tableView.reloadData()
    }
}

// MARK: - UISearchBarDelegate Methods

extension CountryViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchText = searchBar.text ?? ""
        view.endEditing(true)
        tableView.reloadData()
    }
}
