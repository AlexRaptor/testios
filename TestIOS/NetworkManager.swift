//
//  NetworkManager.swift
//  TestIOS
//
//  Created by Александр Селиванов on 15/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

class NetworkManager {
    
    static func sendRequest<T: Decodable>(url: String, completion: @escaping (T?, Error?) -> Void) {
        
        // можно прикрутить Alamofire или ещё что,
        // но думаю, что в рамках данной задачи,
        // можно обойтись стандантными либами
        
        guard let url = URL(string: url) else { return }

        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) -> Void in

            // можно, конечно кастануть response к HTTPResponse-у и работать со statusCode-ами
            if error == nil {
                guard let data = data else { return }

                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let decodedObject = try decoder.decode(T.self, from: data)

                    completion(decodedObject, nil)
                } catch let error {
                    completion(nil, error)
                }
            } else {
                completion(nil, error)
            }
        }.resume()
    }
}
