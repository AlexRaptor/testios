//
//  UnknownAPI.swift
//  TestIOS
//
//  Created by Александр Селиванов on 16/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

protocol UnknownAPIProtocol {
    
    func process(phoneNumber: String, callback: @escaping ([String]?, Error?) -> Void )
}
