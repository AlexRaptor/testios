//
//  PhoneInfoViewController.swift
//  TestIOS
//
//  Created by Александр Селиванов on 16/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit

class PhoneInfoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    var phoneNumber: String?
    var country: String?
    var phoneInfo: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "PhoneInfoCell")
        
        phoneNumberLabel.text = phoneNumber
        countryLabel.text = country
        
        customizeUI()
    }
    
    private func customizeUI() {
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource Methods

extension PhoneInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phoneInfo?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneInfoCell", for: indexPath)
        
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.text = phoneInfo?[indexPath.row]
                
        return cell
    }
}
